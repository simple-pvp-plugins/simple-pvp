package com.simplepvp;

import com.simplepvp.commands.*;
import com.simplepvp.kits.KitMenuEvents;
import com.simplepvp.kits.KitsManager;
import com.simplepvp.menu.MenuGenerator;
import com.simplepvp.util.SimpleGlow;
import com.simplepvp.util.SimpleUtil;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Simple PVP is a plugin designed to be simple to use, update, and make games for.
 * This plugin handles a bulk of the work by managing games, the game navigation, kits, and player commands.
 * This plugin does not offer world protection so a plugin such as World Guard will be needed to protect spawns and game play areas.
 *
 * To make a game, the game plugin must register each game instance (aka map) with Simple PVP to the system.
 * Every game plugin requires a few things. A game class, and a map config class, each of which would extend to Simple PVP's Game and MapConfig classes.
 * The game class will be an instance of a game map and perform all the mechanics of the game. The map config class will store map specific information needed
 * to make a unique game instance such as spawn location.
 *
 * To use kits, your game plugin will need to create a kits object, then register that object to Simple PVP.
 * From there, operators can use the /kit command to edit kits for the game. It is very [ IMPORTANT ] if a game uses kits, that it
 * calls the removePlayerKit() method when the player leaves the game or else the kits class does not know when to release the player's kit data.
 *
 * More information on how to create a game for Simple PVP will be available in the future. Since the source code is
 * available to you, you can study the already available Simple PVP game code in the meantime.
 */

public class SimplePVP extends JavaPlugin implements Listener {

    private static Plugin plugin;
    private static SimpleConfig simpleConfig;
    private static KitsManager kitsManager;
    private static KitCommand kitCommand;
    private static MenuGenerator menuGenerator;
    private static final HashMap<Player, Game> playerGames = new HashMap<>();

    /*------------------------------------------------------------------------------------------
                                     Enable / Disable Methods
     -----------------------------------------------------------------------------------------*/

    /**
     * Called when the plugin is enabled. This performs all the essential functions needed when the plugin starts up for
     * the first time.
     */
    @Override
    public void onEnable() {
        plugin = this;

        // Add a glow enchant that makes items glow in inventory.
        if (Enchantment.getByKey(new NamespacedKey(getPlugin(), "glow")) == null) {
            try {
                Field field = Enchantment.class.getDeclaredField("acceptingNew");
                field.setAccessible(true);
                field.set(null, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Enchantment.registerEnchantment(new SimpleGlow(new NamespacedKey(plugin, "glow")));
        }

        simpleConfig = new SimpleConfig(); // Load the config.
        menuGenerator = new MenuGenerator(); // Create a menu generator that is used in the lobby.
        getServer().getPluginManager().registerEvents(this, this); // Register this plugin's events.
        getServer().getPluginManager().registerEvents(new KitMenuEvents(), this); // Register kit menu events.

        kitsManager = new KitsManager();
        kitCommand = new KitCommand();

        Objects.requireNonNull(this.getCommand("kit")).setExecutor(kitCommand); // Register kit command.
        Objects.requireNonNull(this.getCommand("leave")).setExecutor(new LeaveCommand()); // Register leave command.
        Objects.requireNonNull(this.getCommand("join")).setExecutor(new JoinCommand()); // Register join command.
        Objects.requireNonNull(this.getCommand("spectateplayer")).setExecutor(new SpectatePlayerCommand()); // Register leave command.
        Objects.requireNonNull(this.getCommand("startgame")).setExecutor(new StartGameCommand()); // Register leave command.

        // Send all players to the lobby when the plugin loads.
        for (Player player : getServer().getOnlinePlayers()) {
            sendToLobby(player);
        }
    }

    /**
     * Called when the plugin is disabled.
     */
    @Override
    public void onDisable() {
        // Send all players to the main spawn when plugin gets disabled.
        for (Player player : getServer().getOnlinePlayers()) {
            sendToLobby(player); // Send all players to the lobby when the plugin gets disabled.
        }
    }

    /*------------------------------------------------------------------------------------------
                                     Main Plugin Methods
     -----------------------------------------------------------------------------------------*/
    /**
     * Register the game with the plugin. The game needs to be removed when the registering plugin becomes disabled.
     * @param game The game to be registered.
     */
    public static void registerGame(Game game) {
        menuGenerator.addGame(game); // Adds a game to the menu generator.
        kitCommand.addGameType(game.getGameName()); // Adds the game type to the kit command.
    }

    /**
     * Removes a game from this plugin.
     * @param game The game to be removed.
     */
    public static void removeGame(Game game) {
        // Remove players from the game being removed.
        for (Player player : game.getPlayers().stream().toList()) { // New array to prevent removing from an array we are iterating over.
            sendToLobby(player);
        }
        menuGenerator.removeGame(game); // Removes a game to the menu generator.
    }

    /**
     * Put a player in a game. This will also remove the player from any previous game or lobby they were in.
     * This will fail if the game rejects the player and the player will be put into the lobby.
     * @param player The player to add.
     * @param game The game the player should be added to.
     */
    public static void putPlayerInGame(Player player, Game game) {
        forceQuitPlayer(player); // Force the player to quit their current game.

        if (game.sendPlayer(player)) {
            // If the player was accepted by the game we are sending them to, we store that game in hashmap, so it is readily available.
            playerGames.put(player, game);
            player.playSound(player.getLocation(), Sound.ENTITY_BAT_TAKEOFF, Float.MAX_VALUE, 1);
        } else {
            // If the game did not accept the player, we send them back to the lobby.
            sendToLobby(player);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, Float.MAX_VALUE, 1);
        }
    }

    /**
     * Sends a player to spectate the game by teleporting them to the game's spawn.
     * @param player The player to make spectate.
     * @param game The game to send the player to.
     */
    public static void sendSpectator(Player player, Game game) {
        SimplePVP.forceQuitPlayer(player); // Remove the player from the lobby and games.
        player.setGameMode(GameMode.SPECTATOR); // Put them in spectator.
        player.teleport(game.getMapConfig().getSpawnLocation()); // Teleport to the game's spawn location.
        player.playSound(player.getLocation(), Sound.ENTITY_BAT_TAKEOFF, Float.MAX_VALUE, 1);
    }

    /**
     * Force a player to quit a or lobby game or lobby they are in.
     * @param player The player to quit.
     */
    public static void forceQuitPlayer(Player player) {
        Game playerGame = playerGames.get(player);
        if (playerGame != null) {
            playerGame.removePlayer(player); // Remove the player from their game if it exists.
            playerGames.remove(player);
        }
        SimpleUtil.cleanPlayer(player); //  Cleanup the player's health, inventory, and other stats.
    }

    /**
     * Send a player to the lobby.
     * @param player The player to send.
     */
    public static void sendToLobby(Player player) {
        forceQuitPlayer(player);
        SimpleUtil.cleanPlayer(player);
        player.setGameMode(GameMode.ADVENTURE);
        player.teleport(simpleConfig.spawnLocation);
        player.getInventory().addItem(menuGenerator.getNavigator());
        player.sendMessage(ChatColor.GRAY + "You were sent to the lobby.");
        player.sendMessage("");
        player.playSound(player.getLocation(), Sound.ENTITY_BAT_TAKEOFF, Float.MAX_VALUE, 1);
    }

    /*------------------------------------------------------------------------------------------
                                    Getter / Setter Methods
     -----------------------------------------------------------------------------------------*/

    public static Plugin getPlugin() {
        return plugin;
    }

    public static SimpleConfig getSimpleConfig() {
        return simpleConfig;
    }

    public static KitsManager getKitsManager() {
        return kitsManager;
    }

    public static HashMap<Player, Game> getPlayerGames() {
        return playerGames;
    }

    /*------------------------------------------------------------------------------------------
                                          Event Handlers
     -----------------------------------------------------------------------------------------*/

    // Send the player to lobby when they join and remove player from teams
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Team team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().getEntryTeam(e.getPlayer().getName());

        if (team != null) {
            team.removeEntry(e.getPlayer().getName());
        }

        sendToLobby(e.getPlayer());
    }

    // Force a player leave games when they quit
    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e) {
        forceQuitPlayer(e.getPlayer());
    }

    // Players shouldn't actually die when playing games but if the game bugs we want to send them to the lobby.
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        if (getSimpleConfig().spawnLocation != null) {
            e.setRespawnLocation(getSimpleConfig().spawnLocation);
        }
        sendToLobby(e.getPlayer());
    }

    // Format the server chat.
    @EventHandler
    public void onPlayerSendMessage(AsyncPlayerChatEvent e) {
        String gameText = "Lobby";
        String nameText = e.getPlayer().getName();
        ChatColor teamColor = ChatColor.GRAY;

        Team team = Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard().getEntryTeam(nameText);

        if (team != null) {
            teamColor = team.getColor();
        }
        Game playerGame = playerGames.get(e.getPlayer());

        if (playerGame != null) {
            gameText = playerGame.getGameName();
        }

        e.setFormat(getSimpleConfig().chatFormat.replace("&z", teamColor.toString()).replace("%game%", gameText).replace("%name%", nameText).replace("%message%", e.getMessage()));
    }
}