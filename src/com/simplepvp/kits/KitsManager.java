package com.simplepvp.kits;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;

public class KitsManager {

    private final HashMap<Player, Inventory> playerMenus = new HashMap<>();
    private final HashMap<String, Kits> gameKits = new HashMap<>();

    /**
     * Register a kits class with the kits manager.
     * This will allow the kits manager to handle the kits menu.
     *
     * @param gameType The game type that the kits are for.
     * @param kits The kits class to register.
     */
    public void registerKits(String gameType, Kits kits) {
        gameKits.put(gameType, kits);
    }

    /**
     * Unregister a kits class from the kits manager.
     *
     * @param gameType The game type that the kits are for.
     */
    public void unregisterKits(String gameType) {
        gameKits.remove(gameType);
    }

    /**
     * Got the kits class that was registered by the indicated game.
     *
     * @param gameType The game type that the kits are for.
     * @return Returns the kits class, null if no class was registered.
     */
    public Kits getKits(String gameType) {
        return gameKits.get(gameType);
    }

    /**
     * Add a player menu to be accessed later.
     * Used to validate an inventory's identity.
     *
     * @param player The player that the inventory belongs to.
     * @param inventory The inventory to store.
     */
    public void addPlayerMenu(Player player, Inventory inventory) {
        playerMenus.put(player, inventory);
    }

    /**
     * Get the player menu associated with a player.
     * Used to validate an inventory's identity.
     *
     * @param player The player that the inventory belongs to.
     * @return Returns the inventory found associated with the player, null if none was found.
     */
    public Inventory getPlayerMenu(Player player) {
        return playerMenus.get(player);
    }

    /**
     * Remove a player menu from the kits manager.
     * Used to prevent a memory leak.
     *
     * @param player The player that owns the inventory to remove.
     */
    public void removePlayerMenu(Player player) {
        playerMenus.remove(player);
    }

    /**
     * Check to see if the inventory is the player's current kits menu.
     * This verifies the identity of the inventory accurately.
     *
     * @param player The player to check to kit menu of.
     * @param inventory The inventory being checked.
     * @return Returns true if the inventory is the player's kit menu, false if it is not the player's kit menu.
     */
    public boolean isPlayerMenu(Player player, Inventory inventory) {
        return inventory.equals(getPlayerMenu(player));
    }

}
