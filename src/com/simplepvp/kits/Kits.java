package com.simplepvp.kits;

import com.simplepvp.SimplePVP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Kits {

    private final FileConfiguration kitConfig;
    private final File configFile;

    private final ItemStack kitMenuItem;
    private final Material kitMenuItemMaterial;
    private final String kitMenuItemName;
    private final boolean kitMenuItemEnchanted;
    private final String kitMenuInventoryTitle;

    private final Random rnd = new Random(System.currentTimeMillis());

    private final HashMap<Player, String> playerKits = new HashMap<>();

    /**
     * Initiate the kit class.
     *
     * @param plugin The plugin that parents these kits.
     */
    public Kits(Plugin plugin) {
        kitMenuInventoryTitle = "Kit Menu";
        kitMenuItemName = ChatColor.GOLD + "Kit Menu";
        kitMenuItemMaterial = Material.CHEST;
        kitMenuItemEnchanted = false;

        kitMenuItem = generateKitMenuItem();

        // Get the kits file config.
        configFile = new File(plugin.getDataFolder().getPath() + "/kits.yml");

        // Make the directory if it does not exist yet.
        if (!configFile.exists()) {
            configFile.getParentFile().mkdir();
            plugin.saveResource("kits.yml", true);
        }

        // Load the config.
        kitConfig = YamlConfiguration.loadConfiguration(configFile);
    }

    /**
     * Initiate the kit class.
     *
     * @param plugin The plugin that parents these kits.
     * @param kitInventoryTitle The title of the inventory shown in the kit menu.
     * @param kitMenuMaterial The material of the item in the player's hot bar.
     * @param enchanted Is the item in the hot bar enchanted?
     * @param kitItemName The name of the item in the hot bar.
     */
    public Kits(Plugin plugin, String kitInventoryTitle, Material kitMenuMaterial, boolean enchanted, String kitItemName) {
        kitMenuInventoryTitle = kitInventoryTitle;
        kitMenuItemName = kitItemName;
        kitMenuItemMaterial = kitMenuMaterial;
        kitMenuItemEnchanted = enchanted;

        kitMenuItem = generateKitMenuItem();

        // Get the kits file config.
        configFile = new File(plugin.getDataFolder().getPath() + "/kits.yml");

        // Make the directory if it does not exist yet.
        if (!configFile.exists()) {
            configFile.getParentFile().mkdir();
            plugin.saveResource("kits.yml", true);
        }

        // Load the config.
        kitConfig = YamlConfiguration.loadConfiguration(configFile);
    }

    /**
     * Save the kit to the config with an item icon.
     *
     * @param name Name of the kit.
     * @param inventory The inventory to store in the kit.
     * @param material The item to be displayed in the kit menu.
     *
     * @throws IOException Thrown when the kit could not be saved.
     */
    public void saveKit(String name, Inventory inventory, Material material) throws IOException {
        ItemStack[] items = inventory.getContents();
        for (int i = 0; i < items.length; i++) {
            ItemStack item = items[i];
            if (item != null) {
                kitConfig.set("kits." + name + ".slot-" + i, item);
            }
        }
        kitConfig.set("kit-icons." + name, material.toString());
        kitConfig.save(configFile);
    }

    /**
     * Delete a kit from the kit from the config.
     *
     * @param name The name of the kit to delete.
     *
     * @throws IOException Thrown when the kit could not be deleted.
     */
    public void deleteKit(String name) throws IOException {
        kitConfig.set("kits." + name, null);
        kitConfig.set("kit-icons." + name, null);
        kitConfig.save(configFile);
    }

    /**
     * Load the kit and give the inventory.
     *
     * @param name The kit to load.
     * @return Returns the inventory with the kit inside.
     */
    public Inventory loadKit(String name) {
        List<String> kits = new ArrayList<>(kitList());
        if (name == null) {
            if (kits.size() > 0) {
                name = kits.get(rnd.nextInt(kits.size()));
            } else {
                Bukkit.getLogger().warning("There is a game that has no kits configured.");
                return Bukkit.createInventory(null, InventoryType.PLAYER);
            }
        }
        ConfigurationSection kit = kitConfig.getConfigurationSection("kits." + name);
        Inventory newInventory = Bukkit.createInventory(null, InventoryType.PLAYER);
        if (kit != null) {
            for (int i = 0; i < newInventory.getSize(); i++) {
                ItemStack slotItem = kit.getItemStack("slot-" + i);
                if (slotItem != null) {
                    newInventory.setItem(i, slotItem);
                }
            }
        }
        return newInventory;
    }

    /**
     * Get the logo for a given kit.
     *
     * @param name The kit to find the icon for.
     * @return Returns The material of the icon.
     */
    public Material loadKitIcon(String name) {
        return Material.valueOf(kitConfig.getString("kit-icons." + name));
    }

    /**
     * Get a set of kit names that are in the config.
     *
     * @return Returns a set of kits.
     */
    public Set<String> kitList() {
        return Objects.requireNonNull(kitConfig.getConfigurationSection("kits")).getKeys(false);
    }

    /**
     * Get a kit menu inventory for a specific player with a selected kit highlighted.
     *
     * @param player The player to get the inventory for.
     * @param selectedKit The selected kit, null if no kit is selected.
     * @return Returns the inventory with the generated kit.
     */
    public Inventory generateKitMenuInventory(Player player, String selectedKit) {
        Inventory inv = Bukkit.createInventory(player, 54, kitMenuInventoryTitle);
        List<String> kits = new ArrayList<>(kitList());

        for (int i = 0; i < Math.min(45, kits.size()); i++) {
            String kit = kits.get(i);
            ItemStack kitItem = new ItemStack(loadKitIcon(kit));
            ItemMeta meta = kitItem.getItemMeta();
            assert meta != null;
            if (kit.equals(selectedKit)) {
                meta.addEnchant(Objects.requireNonNull(Enchantment.getByKey(new NamespacedKey(SimplePVP.getPlugin(), "glow"))), 1, true);
                meta.setLore(new ArrayList<>(Collections.singleton(ChatColor.GRAY + "(Selected)")));
            }
            meta.setDisplayName(ChatColor.GOLD + kit);
            kitItem.setItemMeta(meta);
            inv.setItem(i, kitItem);
        }

        ItemStack kitItem = new ItemStack(Material.DISPENSER);
        ItemMeta meta = kitItem.getItemMeta();
        assert meta != null;

        if (selectedKit == null) {
            meta.addEnchant(Objects.requireNonNull(Enchantment.getByKey(new NamespacedKey(SimplePVP.getPlugin(), "glow"))), 1, true);
            meta.setLore(new ArrayList<>(Collections.singleton(ChatColor.GRAY + "(Selected)")));
        }

        meta.setDisplayName(ChatColor.GOLD + "Random Kit");
        kitItem.setItemMeta(meta);
        inv.setItem(49, kitItem);

        kitItem = new ItemStack(Material.ARROW);
        meta = kitItem.getItemMeta();
        assert meta != null;
        meta.setDisplayName(ChatColor.GRAY + "Exit");
        kitItem.setItemMeta(meta);
        inv.setItem(53, kitItem);
        return inv;
    }

    /**
     * Get the kit menu item that is used to access the kit menu.
     *
     * @return Returns the kit menu item.
     */
    public ItemStack getKitMenuItem() {
        return kitMenuItem;
    }

    public void setPlayerKit(Player player, String kit) {
        playerKits.put(player, kit);
    }

    public String getPlayerKit(Player player) {
        return playerKits.get(player);
    }

    public void removePlayerKit(Player player) {
        playerKits.remove(player);
    }

    public void equipKit(Player player) {
        player.getInventory().clear();
        player.getInventory().setContents(loadKit(getPlayerKit(player)).getContents());
    }

    // Generate the kit menu item.
    private ItemStack generateKitMenuItem() {
        assert kitMenuItemMaterial != null;
        ItemStack itemStack = new ItemStack(kitMenuItemMaterial);
        ItemMeta itemMeta = itemStack.getItemMeta();
        assert itemMeta != null;
        itemMeta.setDisplayName(kitMenuItemName);
        if (kitMenuItemEnchanted) {
            itemMeta.addEnchant(Objects.requireNonNull(Enchantment.getByKey(new NamespacedKey(SimplePVP.getPlugin(), "glow"))), 1, true);
        }
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}