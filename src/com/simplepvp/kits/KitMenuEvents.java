package com.simplepvp.kits;

import com.simplepvp.Game;
import com.simplepvp.SimplePVP;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;

public class KitMenuEvents implements Listener {

    // Detect when a player tries to open a kit menu.
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Game playerGame = SimplePVP.getPlayerGames().get(event.getPlayer());
        if (playerGame != null && event.getItem() != null &&
                (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {
            Kits playerKits = SimplePVP.getKitsManager().getKits(playerGame.getGameName());
            if (playerKits != null && event.getItem().equals(SimplePVP.getKitsManager().getKits(playerGame.getGameName()).getKitMenuItem())) {
                /* Generate the kit menu, open it for the player, and add it to the kit manager.
                   Order is important because opening the inventory closes the other one that will remove the saved inventory. */
                Inventory newInventory = SimplePVP.getKitsManager().getKits(playerGame.getGameName()).generateKitMenuInventory(event.getPlayer(),
                        SimplePVP.getKitsManager().getKits(playerGame.getGameName()).getPlayerKit(event.getPlayer()));
                event.getPlayer().openInventory(newInventory);
                SimplePVP.getKitsManager().addPlayerMenu(event.getPlayer(), newInventory);

                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLOCK_CHEST_OPEN, Float.MAX_VALUE, 1);

                event.setCancelled(true);
            }
        }
    }

    // Remove the player menu to prevent memory problems.
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        SimplePVP.getKitsManager().removePlayerMenu((Player) event.getPlayer());
    }

    // Handle player interaction with the kit menu.
    @EventHandler
    public void onInventoryInteract(InventoryClickEvent event) {

        if (event.getWhoClicked() instanceof Player player) {

            Game playerGame = SimplePVP.getPlayerGames().get(player);

            // Ensure that the game and the clicked inventory exist.
            event.getInventory();
            if (playerGame != null) {

                // See if this is the player kit menu.
                if (SimplePVP.getKitsManager().isPlayerMenu(player, event.getInventory())) {

                    ItemStack item = event.getCurrentItem();
                    event.setCancelled(true);

                    // We don't want to do anything if they clicked on air.
                    if (item != null && !item.getType().equals(Material.AIR)  && event.isLeftClick()) {

                        // Get the un-formatted name of the kit selected.
                        String cleanName = null;
                        if (item.hasItemMeta()) {
                            cleanName = ChatColor.stripColor(Objects.requireNonNull(Objects.requireNonNull(event.getCurrentItem()).getItemMeta()).getDisplayName());
                        }

                        // If they click the exit button, close the inventory.
                        if (event.getSlot() == 53) {
                            player.closeInventory();
                            event.setCancelled(true);
                            SimplePVP.getKitsManager().removePlayerMenu(player);
                            ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.UI_BUTTON_CLICK, Float.MAX_VALUE, 1);
                            return;

                            // If they click the random kit button, set the kit null.
                        } else if (event.getSlot() == 49) {
                            SimplePVP.getKitsManager().getKits(playerGame.getGameName()).setPlayerKit(player, null);
                            Inventory newInventory = SimplePVP.getKitsManager().getKits(playerGame.getGameName()).generateKitMenuInventory(player, null);
                            player.openInventory(newInventory);
                            SimplePVP.getKitsManager().addPlayerMenu(player, newInventory);
                            ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.UI_BUTTON_CLICK, Float.MAX_VALUE, 1);

                            // Set the kit that is selected.
                        } else if (event.getSlot() < 45) {
                            SimplePVP.getKitsManager().getKits(playerGame.getGameName()).setPlayerKit(player, cleanName);
                            Inventory newInventory = SimplePVP.getKitsManager().getKits(playerGame.getGameName()).generateKitMenuInventory(player, cleanName);
                            player.openInventory(newInventory);
                            SimplePVP.getKitsManager().addPlayerMenu(player, newInventory);
                            ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.UI_BUTTON_CLICK, Float.MAX_VALUE, 1);
                        }
                    }
                    event.setCancelled(true);
                }
            }
        }
    }

}
