package com.simplepvp.commands;

import com.simplepvp.SimplePVP;
import com.simplepvp.kits.Kits;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.*;

public class KitCommand implements CommandExecutor, TabCompleter {

    private final List<String> subcommands = new ArrayList<>(Arrays.asList("setkit", "getkit", "kitlist", "delkit"));
    private final HashSet<String> gameTypes = new HashSet<>();

    public KitCommand() {

    }

    public void addGameType(String gameType) {
        gameTypes.add(gameType);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "Please select a game type!");
            return true;
        }

        Kits gameKits = SimplePVP.getKitsManager().getKits(args[0]);

        if (gameKits != null) {

            if (args.length == 1) {
                sender.sendMessage(ChatColor.RED + "Please select a sub command!");
                return true;
            }

            switch (args[1]) {
                case "setkit" -> {
                    if (args.length < 4) {
                        sender.sendMessage(ChatColor.RED + "This subcommand requires two arguments. Kit name and kit icon item.");
                        break;
                    }
                    try {
                        Material mat = Material.getMaterial(args[3].split(":")[1].toUpperCase());
                        if (mat == null) {
                            sender.sendMessage(ChatColor.RED + "The material " + args[3] + " could not be found!");
                            return true;
                        }
                        gameKits.deleteKit(args[2]);
                        gameKits.saveKit(args[2], ((Player) sender).getInventory(), mat);
                        sender.sendMessage(ChatColor.GOLD + "Kit " + args[2] + " has been saved.");
                    } catch (IOException e) {
                        sender.sendMessage(ChatColor.GOLD + "File error occurred, kit could not be saved.");
                    }
                }
                case "getkit" -> {
                    if (args.length < 3) {
                        sender.sendMessage(ChatColor.RED + "This subcommand requires a kit name.");
                        break;
                    }
                    sender.sendMessage(ChatColor.GOLD + "Kit " + args[2] + " has been equipped.");
                    ((Player) sender).getInventory().setContents(gameKits.loadKit(args[2]).getContents());
                }
                case "kitlist" -> {
                    sender.sendMessage(ChatColor.GOLD + "List of kits:");
                    StringBuilder builder = new StringBuilder(ChatColor.YELLOW.toString());
                    for (String kitName : gameKits.kitList()) {
                        builder.append(kitName);
                        builder.append(" | ");
                    }
                    sender.sendMessage(builder.toString());
                }
                case "delkit" -> {
                    if (args.length < 3) {
                        sender.sendMessage(ChatColor.RED + "This subcommand requires a kit name.");
                        break;
                    }
                    try {
                        gameKits.deleteKit(args[2]);
                        sender.sendMessage(ChatColor.GOLD + "Kit " + args[2] + " has been deleted.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            sender.sendMessage(ChatColor.RED + "The specified game does not use kits.");
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> tabList = new ArrayList<>();

        if (!(sender instanceof Player)) {
            return tabList;
        }

        if (args.length == 1) {
            return new ArrayList<>(gameTypes);
        }

        if (args.length == 2) {
            return subcommands;
        }

        switch (args[1]) {
            case "setkit" -> {
                if (args.length < 4) {
                    break;
                }
                for (Material material : Material.values()) {
                    tabList.add(material.getKey().toString());
                }
            }
            case "getkit", "delkit" -> {
                Kits gameKits = SimplePVP.getKitsManager().getKits(args[0]);
                if (gameKits != null) {
                    tabList.addAll(gameKits.kitList());
                }
            }
        }
        return tabList;
    }
}
