package com.simplepvp.commands;

import com.simplepvp.SimplePVP;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (sender instanceof Player player) {
            SimplePVP.sendToLobby(player);
        }
        return true;
    }
}
