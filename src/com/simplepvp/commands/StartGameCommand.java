package com.simplepvp.commands;

import com.simplepvp.Game;
import com.simplepvp.SimplePVP;;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartGameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (sender instanceof Player player) {
            Game game = SimplePVP.getPlayerGames().get(player);

            if (game != null) {
                if (!game.forceStartGame()) {
                    sender.sendMessage(ChatColor.RED + "Game is not able to start! Make sure game is ready to start before running this command.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "You must be in a game to run this command.");
            }
        }
        return true;
    }
}
