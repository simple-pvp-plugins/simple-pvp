package com.simplepvp.commands;

import com.simplepvp.Game;
import com.simplepvp.SimplePVP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class JoinCommand implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (sender instanceof Player player) {
            if (args.length < 1) {
                sender.sendMessage(ChatColor.RED + "Please select a player to join.");
                return true;
            }

            Player subject = Bukkit.getServer().getPlayer(args[0]);
            if (subject != null) {

                if (subject.equals(player)) {
                    sender.sendMessage(ChatColor.RED + "You can not join yourself.");
                    return true;
                }

                Game subjectGame = SimplePVP.getPlayerGames().get(subject);
                if (subjectGame != null) {
                    if (subjectGame.hasPlayer(player)) {
                        sender.sendMessage(ChatColor.RED + "You are already in a game with " + subject.getName() + ".");
                        return true;
                    }
                    SimplePVP.putPlayerInGame(player, subjectGame);
                } else {
                    sender.sendMessage(ChatColor.RED + "The player specified is not in a game.");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "The player specified is not online.");
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> tabList = new ArrayList<>();

        for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()) {
            if (!onlinePlayer.getName().equals(sender.getName())) {
                tabList.add(onlinePlayer.getName());
            }
        }

        return tabList;
    }
}
