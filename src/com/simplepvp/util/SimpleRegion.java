package com.simplepvp.util;

public class SimpleRegion {

    private final SimplePoint pos1, pos2;

    /**
     * Creates a simple region between two points.
     * @param p1 The first point of the region.
     * @param p2 The second point of the region.
     */
    public SimpleRegion (SimplePoint p1, SimplePoint p2) {
        pos1 = new SimplePoint(Math.min(p1.x(), p2.x()), Math.min(p1.y(), p2.y()), Math.min(p1.z(), p2.z()));
        pos2 = new SimplePoint(Math.max(p1.x(), p2.x()), Math.max(p1.y(), p2.y()), Math.max(p1.z(), p2.z()));
    }

    /**
     * Creates a simple region between two points.
     * @param x1 The x coordinate of the first point of the region.
     * @param y1 The y coordinate of the first point of the region.
     * @param z1 The z coordinate of the first point of the region.
     * @param x2 The x coordinate of the second point of the region.
     * @param y2 The y coordinate of the second point of the region.
     * @param z2 The z coordinate of the second point of the region.
     */
    public SimpleRegion (int x1, int y1, int z1, int x2, int y2, int z2) {
        pos1 = new SimplePoint(Math.min(x1, x2), Math.min(y1, y2), Math.min(z1, z2));
        pos2 = new SimplePoint(Math.max(x1, x2), Math.max(y1, y2), Math.max(z1, z2));
    }

    /**
     * Returns whether a point is in the region.
     * @param p The point to check.
     * @return Returns true if the point is in the region. Returns false if it isn't in the region.
     */
    public boolean inRegion(SimplePoint p) {
        return (p.x() >= pos1.x() && p.x() <= pos2.x() && p.y() >= pos1.y() && p.y() <= pos2.y() && p.z() >= pos1.z() && p.z() <= pos2.z());
    }

    /**
     * Returns whether a point is in the region.
     * @param x The x coordinate of the point to check.
     * @param y The y coordinate of the point to check.
     * @param z The z coordinate of the point to check.
     * @return Returns true if the point is in the region. Returns false if it isn't in the region.
     */
    public boolean inRegion(int x, int y, int z) {
        return (x >= pos1.x() && x <= pos2.x() && y >= pos1.y() && y <= pos2.y() && z >= pos1.z() && z <= pos2.z());
    }

    /**
     * Get the volume of the region.
     * @return Returns the volume of the region.
     */
    public int getVolume() {
        return (pos2.x() - pos1.x()) * (pos2.y() - pos1.y()) * (pos2.z() - pos1.z());
    }

    /**
     * Get the first point of the region. This is the lower North-West corner.
     * @return The first point of the region.
     */
    public SimplePoint getPos1() {
        return pos1;
    }

    /**
     * Get the second point of the region. This is the upper South-East corner.
     * @return The second point of the region.
     */
    public SimplePoint getPos2() {
        return pos2;
    }
}
