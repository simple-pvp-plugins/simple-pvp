package com.simplepvp.util;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.util.List;

public class SimpleUtil {

    /**
     * Cleans up the player's stats and inventory.
     * @param player The player to clean.
     */
    public static void cleanPlayer(Player player) {
        player.getInventory().clear();
        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
        player.setHealth(20);
        player.setFoodLevel(40);
        player.setFireTicks(0);
        player.setExp(0);
        player.setTotalExperience(0);
    }

    /**
     * Sends a group of players a message in chat.
     * @param players The player group.
     * @param message The message.
     */
    public static void sendPlayersMessage(List<Player> players, String message) {
        for (Player player : players) {
            player.sendMessage(message);
        }
    }

    /**
     * Shows a group of players a title.
     * @param players The player group.
     * @param m1 The title.
     * @param m2 The sub title.
     * @param l1 The fade in time in ticks.
     * @param l2 The showing time in ticks.
     * @param l3 The fade out time in ticks.
     */
    public static void sendPlayersTitle(List<Player> players, String m1, String m2, int l1, int l2, int l3) {
        for (Player player :  players) {
            player.sendTitle(m1, m2, l1, l2, l3);
        }
    }

    /**
     * Sends a group of players an action bar message.
     * @param players The player group.
     * @param message The message.
     */
    public static void sendPlayersActionBar(List<Player> players, String message) {
        for (Player player :  players) {
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
        }
    }

    /**
     * Plays a global sound to a group of players.
     * @param players The player group.
     * @param sound The sound to play.
     * @param pitch The sound pitch.
     */
    public static void playPlayersGlobalSound(List<Player> players, Sound sound, float pitch) {
        for (Player player :  players) {
            player.playSound(player.getLocation(), sound, Float.MAX_VALUE, pitch);
        }
    }

    /**
     * Plays a local sound to a group of players.
     * @param players  The player group.
     * @param location The sound location.
     * @param sound The sound to play.
     * @param volume The sound volume.
     * @param pitch The sound pitch.
     */
    public static void playPlayersLocalSound(List<Player> players, Location location, Sound sound, float volume, float pitch) {
        for (Player player :  players) {
            player.playSound(location, sound, volume, pitch);
        }
    }

}
