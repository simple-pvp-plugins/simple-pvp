package com.simplepvp.util;

public record SimplePoint(int x, int y, int z) { }
