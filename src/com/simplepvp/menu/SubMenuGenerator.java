package com.simplepvp.menu;

import com.simplepvp.Game;
import com.simplepvp.SimplePVP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class SubMenuGenerator implements Listener {
    private final String gameType;
    private final Material icon;
    private final List<Game> games =  new ArrayList<>();
    private final HashMap<String, Game> menuButtons = new HashMap<>();
    private final Set<Inventory> openMenuSet = new HashSet<>();
    private final List<String> buttonLore = new ArrayList<>();

    /**
     * Default constructor for SubMenuGenerator
     * @param gameType The game type that all maps in this sub menu have.
     * @param icon The item to display that opens up this menu.
     */
    public SubMenuGenerator(String gameType, Material icon) {
        this.gameType = gameType;
        this.icon = icon;
        SimplePVP.getPlugin().getServer().getPluginManager().registerEvents(this, SimplePVP.getPlugin());
        buttonLore.add(ChatColor.GRAY + "Left click to join game.");
        buttonLore.add(ChatColor.GRAY + "Right click to spectate game.");
    }

    /**
     * Generate a menu button for this sub-menu.
     * @return Returns the button as an ItemStack.
     */
    public ItemStack generateMainButton() {
        // Create the item and set the name.
        ItemStack buttonItem = new ItemStack(icon, 1);
        ItemMeta buttonMeta = buttonItem.getItemMeta();
        assert buttonMeta != null;
        buttonMeta.setDisplayName(ChatColor.GOLD + gameType);
        buttonItem.setItemMeta(buttonMeta);
        return buttonItem;
    }

    /**
     * Opens the sub-menu for a player.
     * @param player The player to show the sub-menu.
     */
    public void openSubMenu(Player player) {
        // Create the inventory and add the buttons.
        Inventory newInventory = Bukkit.createInventory(null, 54, "Map Menu");
        for (Map.Entry<String, Game> entry : menuButtons.entrySet()) {
            Game game = entry.getValue();
            ItemStack button = new ItemStack(Material.MAP);
            ItemMeta buttonMeta = button.getItemMeta();
            assert buttonMeta != null;
            List<String> thisButtonLore = new ArrayList<>(buttonLore);
            if (game.isGameOpen()) {
                buttonMeta.setDisplayName(ChatColor.GOLD + entry.getKey());
                thisButtonLore.add(ChatColor.DARK_GRAY + "(" + ChatColor.GRAY + game.getPlayers().size() + ChatColor.DARK_GRAY +
                        "/" + ChatColor.GRAY + game.getMapConfig().getMaxPlayers() + ChatColor.DARK_GRAY + ")");
            } else {
                buttonMeta.setDisplayName(ChatColor.RED + entry.getKey());
                thisButtonLore.add(ChatColor.DARK_RED + "(" + ChatColor.RED + game.getPlayers().size() + ChatColor.DARK_RED +
                        "/" + ChatColor.RED + game.getMapConfig().getMaxPlayers() + ChatColor.DARK_RED + ")");
            }
            buttonMeta.setLore(thisButtonLore);
            button.setItemMeta(buttonMeta);
            newInventory.addItem(button);
        }
        player.openInventory(newInventory);
        openMenuSet.add(newInventory); // Store the inventory to compare to later.
    }

    /**
     * Add a game to this sub-menu. It will only add if the game is the right type for this sub-menu.
     * @param game The game to add.
     */
    public void addGame(Game game) {
        // Make sure the game is of the same game type assigned to this sub-menu.
        if (game.getGameName().equals(gameType)) {
            games.add(game);
            // Create a sub-menu button for the map on this game.
            menuButtons.put(game.getMapConfig().getMapName(), game);
        }
    }

    /**
     * Removes a game from this sub-menu.
     * @param game The game to remove.
     */
    public void removeGame(Game game) {
        games.remove(game);
        // Remove the menu button
        // This can be slow but it is okay since it is only run when the game SimplePVP.getPlugin()s become disabled.
        for (Map.Entry<String, Game> entry : menuButtons.entrySet()) {
            if (game.equals(entry.getValue())) {
                menuButtons.remove(entry.getKey());
                return; // Only one should exist, so we can exit early to save time.
            }
        }
    }

    /**
     * Returns if the game list in this sub-menu is empty.
     * @return Returns true if empty. Returns false if not empty.
     */
    public boolean isEmpty() {
        return games.isEmpty();
    }

    public String getGameType() {
        return gameType;
    }

    @EventHandler
    public void onInventoryInteract(InventoryClickEvent e) {
        // If the inventory is an open menu we want to not let the player move the buttons.
        if (openMenuSet.contains(e.getInventory()) && e.getCurrentItem() != null) {
            e.setCancelled(true);
            String itemName = ChatColor.stripColor(Objects.requireNonNull(e.getCurrentItem().getItemMeta()).getDisplayName());
            if (menuButtons.containsKey(itemName)) {
                // If left click send to game, if right click spectate game.
                if (e.getClick().isLeftClick()) {
                    SimplePVP.putPlayerInGame((Player) e.getWhoClicked(), menuButtons.get(itemName));
                    e.getWhoClicked().closeInventory();
                } else if (e.getClick().isRightClick()) {
                    SimplePVP.sendSpectator((Player) e.getWhoClicked(), menuButtons.get(itemName));
                    e.getWhoClicked().closeInventory();
                }
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        // Remove now unneeded inventories when they become closed.
        openMenuSet.remove(e.getInventory());
    }
}
