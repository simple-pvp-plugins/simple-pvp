package com.simplepvp.menu;

import com.simplepvp.Game;
import com.simplepvp.SimplePVP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class MenuGenerator implements Listener {
    
    private final ItemStack navigator;
    private final HashMap<String, SubMenuGenerator> subMenuGeneratorHashMap = new HashMap<>();
    private final HashMap<ItemStack, SubMenuGenerator> menuButtons = new HashMap<>();
    private final HashMap<SubMenuGenerator, Integer> menuButtonPositions = new HashMap<>();
    private final Set<Inventory> openMenuSet = new HashSet<>();
    private final FileConfiguration navigatorConfig;

    /**
     * The default constructor for the MenuGenerator class.
     */
    public MenuGenerator() {
        SimplePVP.getPlugin().getServer().getPluginManager().registerEvents(this, SimplePVP.getPlugin()); // Register events.

        navigatorConfig = loadNavigatorConfig();

        navigator = new ItemStack(Material.COMPASS, 1); // Create the navigator.
        ItemMeta navMeta = navigator.getItemMeta();
        assert navMeta != null;
        navMeta.setDisplayName(ChatColor.GOLD + "Navigator");
        navigator.setItemMeta(navMeta);
    }

    private FileConfiguration loadNavigatorConfig() {
        File customConfigFile = new File(SimplePVP.getPlugin().getDataFolder(), "navigator.yml");
        if (!customConfigFile.exists()) {
            customConfigFile.getParentFile().mkdirs();
            SimplePVP.getPlugin().saveResource("navigator.yml", false);
        }

        FileConfiguration customConfig = new YamlConfiguration();
        try {
            customConfig.load(customConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        return customConfig;
    }

    /**
     * Add a game to the menu. All games of the same type will be listed as one with a map sub-menu.
     * @param game The game to add to the menu.
     */
    public void addGame(Game game) {
        String gameType = game.getGameName(); // Get the game type of this game
        SubMenuGenerator subMenuGenerator = subMenuGeneratorHashMap.get(gameType); // Get the sub menu generator of this game type.

        if (subMenuGenerator != null) {
            // If the generator exists, add this game.
            subMenuGenerator.addGame(game);
        } else {
            // If the generator does not exist, create a new one and add the game.
            subMenuGenerator = new SubMenuGenerator(gameType, game.getGameIcon());
            subMenuGeneratorHashMap.put(gameType, subMenuGenerator);
            subMenuGenerator.addGame(game);
            menuButtons.put(subMenuGenerator.generateMainButton(), subMenuGenerator);

            // Find the slot the game should be put in.
            for (int i = 0; i < 54; i++) {
                String navSlotGame = navigatorConfig.getString("" + i);

                if (navSlotGame != null && navSlotGame.equalsIgnoreCase(gameType)) {
                    // Put the game in this item slot as defined by the config.
                    menuButtonPositions.put(subMenuGenerator, i);
                    break;
                }
            }
        }
    }

    /**
     * Remove a game from the menu.
     * @param game The game to remove.
     */
    public void removeGame(Game game) {
        String gameType = game.getGameName();
        SubMenuGenerator subMenuGenerator = subMenuGeneratorHashMap.get(gameType); // Get the sub menu generator of this game type.

        if (subMenuGenerator != null) {
            // Make sure it isn't null
            subMenuGenerator.removeGame(game); // Remove the game.

            if (subMenuGenerator.isEmpty()) {
                // If it is now empty we can remove it.
                subMenuGeneratorHashMap.remove(gameType);

                // Remove the buttons linked to the sub menu generator.
                // This can be slow, but it is okay since it is only run when the game SimplePVP.getPlugin()s become disabled.
                for (Map.Entry<ItemStack, SubMenuGenerator> entry : menuButtons.entrySet()) {
                    if (subMenuGenerator.equals(entry.getValue())) {
                        menuButtonPositions.remove(subMenuGenerator);
                        menuButtons.remove(entry.getKey());
                        return; // Only one should exist, so we can exit early to save time.
                    }
                }
            }
        }
    }

    /**
     * Open the main menu for a player.
     * @param player The player to show the menu.
     */
    public void openMainMenu(Player player) {
        Inventory newInventory = Bukkit.createInventory(null, 54, "Game Menu"); // Create a new double chest inventory.
        // Add the buttons
        for (Map.Entry<ItemStack, SubMenuGenerator> entry : menuButtons.entrySet()) {
            newInventory.setItem(menuButtonPositions.get(entry.getValue()), entry.getKey());
        }
        player.openInventory(newInventory); // Show the inventory to the player.
        openMenuSet.add(newInventory); // Store the inventory for later use in inventory events.
    }

    /**
     * Get the navigator item.
     * @return The navigator item.
     */
    public ItemStack getNavigator() {
        return navigator;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        // Open the menu if the navigator was right-clicked.
        if (e.getItem() != null && e.getItem().equals(navigator) && (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {
            e.setCancelled(true);
            openMainMenu(e.getPlayer());
        }
    }

    @EventHandler
    public void onInventoryInteract(InventoryClickEvent e) {
        // If the inventory is an open menu we want to not let the player move the buttons.
        if (openMenuSet.contains(e.getInventory())) {
            e.setCancelled(true); // Stop the event.
            // If the button is a menu button, open that sub menu.
            if (menuButtons.containsKey(e.getCurrentItem()) && e.getClick().isLeftClick()) {
                menuButtons.get(e.getCurrentItem()).openSubMenu((Player) e.getWhoClicked());
                ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.UI_BUTTON_CLICK, Float.MAX_VALUE, 1);
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        // Remove now unneeded inventories when they become closed.
        openMenuSet.remove(e.getInventory());
    }
}
