package com.simplepvp;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Objects;

/**
 * Handles the base configuration for Simple PVP.
 */
public class SimpleConfig {

    public final Location spawnLocation;
    public final String chatFormat;

    public SimpleConfig () {
        SimplePVP.getPlugin().saveDefaultConfig();
        FileConfiguration config = SimplePVP.getPlugin().getConfig();

        // Add defaults.
        config.addDefault("spawn-location", new Location(Bukkit.getWorlds().get(0), 0.5, 64, 0.5, 0, 0));
        config.addDefault("chat-format", "&8[&7%game%&8] &z%name% &r%message%");

        // Save defaults.
        config.options().copyDefaults(true);
        SimplePVP.getPlugin().saveConfig();

        // Load config.
        spawnLocation = config.getLocation("spawn-location");
        chatFormat = ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(config.getString("chat-format")));
    }

}