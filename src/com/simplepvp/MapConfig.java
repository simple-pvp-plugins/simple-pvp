package com.simplepvp;

import org.bukkit.Location;

/**
 * Map configs contain data specific to a game to run on a map.
 * Data to add can spawn points, map regions, or other unique map data.
 */
public abstract class MapConfig {

    private final String mapName;
    private final int minPlayers, maxPlayers;
    private final Location spawnLocation;

    /**
     * The default constructor for the MapConfig class.
     * @param mapName The name of the map that this config is for.
     * @param maxPlayers The maximum players the map can have. (For team based games it should be an even number)
     * @param spawnLocation The default spawn location for the game. Usually a waiting area before the game starts.
     */
    public MapConfig(String mapName, int minPlayers, int maxPlayers, Location spawnLocation) {
        this.mapName = mapName;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.spawnLocation = spawnLocation;
    }

    public String getMapName() {
        return mapName;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public Location getSpawnLocation() {
        return spawnLocation;
    }

}