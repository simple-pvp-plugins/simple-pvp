package com.simplepvp;

import com.simplepvp.util.SimpleUtil;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Games should only affect players that are in its player list. Code your event listeners in your game object to
 * not affect other games. This can be archived by checking if the event was triggered by a player in the game or if
 * the event was triggered in the map's play region.
 *
 * The game should be self resetting so any broken or placed blocks or entities must be fixed before the next game.
 */
public abstract class Game implements Listener {
    private final String gameName;
    private final Material gameIcon;
    private final List<Player> players = new ArrayList<>();
    private final MapConfig mapConfig;
    private BukkitTask countTask, resetTask;
    private int countDownTime = 30 ;
    protected boolean gameOpen, gameCountDown, gameStarted, gameEnded;

    /**
     * The default constructor for the Game class.
     * @param gameName The name of the game, also known as game type. Games of the same name will be grouped together in the game menu.
     * @param gameIcon The item to represent the game's icon. This will be used for the game menu.
     * @param mapConfig The map config for this particular game.
     */
    public Game(String gameName, Material gameIcon, MapConfig mapConfig) {
        // Set the parameters.
        this.gameName = gameName;
        this.gameIcon = gameIcon;
        this.mapConfig = mapConfig;

        // Set the default startup flags.
        gameOpen = true;
        gameCountDown = false;
        gameStarted = false;
        gameEnded = false;

        // Register events with the plugin manager.
        SimplePVP.getPlugin().getServer().getPluginManager().registerEvents(this, SimplePVP.getPlugin());
    }

    /**
     * Attempts to send a player to this game. When overriding this method, only execute code if the super of this method
     * returns a true value.
     * @param player The player to send to the game.
     * @return Returns true if sending the player was successful, false if it failed.
     */
    public boolean sendPlayer(Player player) {
        // Only send the player if the game is open. Return false since the player was never sent.
        if (!gameOpen) {
            player.sendMessage(ChatColor.RED + "The game you are trying to join is no longer open.");
            return false;
        }

        // Add the player to the players list for this game.
        players.add(player);

        // Teleport the player to spawn, update their game mode, and notify the game players of the new player.
        player.teleport(getMapConfig().getSpawnLocation());
        player.setGameMode(GameMode.ADVENTURE);
        player.sendMessage(ChatColor.BLUE + "Putting you in " + getGameName() + " in map " + getMapConfig().getMapName() + ".");
        player.sendMessage("");
        SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GRAY + player.getName() + " has joined the game. " + ChatColor.DARK_GRAY
                + "(" + getPlayers().size() + "/" + getMapConfig().getMaxPlayers() + ")");

        // Start the game countdown if there are enough players, else notify the player that more are needed.
        if (getPlayers().size() >= getMapConfig().getMinPlayers()) {
            startCountdown();
        } else {
            if (getMapConfig().getMinPlayers() - getPlayers().size() == 1) {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.RED + "" +
                        (getMapConfig().getMinPlayers() - getPlayers().size()) + " more player needed to start the game!");
            } else {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.RED + "" +
                        (getMapConfig().getMinPlayers() - getPlayers().size()) + " more players needed to start the game!");
            }
        }

        // Close the game if the player limit is reached.
        if (getPlayers().size() >= getMapConfig().getMaxPlayers()) {
            gameOpen = false;
        }

        return true;
    }

    /**
     * Removes a player from the game. Returns a value whether the player was in the game before this was called.
     * @param player The player to remove.
     * @return Returns true if the player was in the game and removed, else returns false if the player wasn't in the game.
     */
    public boolean removePlayer(Player player) {

        if (players.remove(player)) {
            // Notify the players in the game the player has left.
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GRAY + player.getName() + " has left the game.");

            // Open the game if the player count drops below the max player count and the game isn't started.
            if (!gameStarted && getPlayers().size() < getMapConfig().getMaxPlayers()) {
                gameOpen = true;
            }

            // Check if the game should be won.
            checkGameWin();

            // Stop the countdown if there are not enough players to start the game.
            if (gameCountDown && getPlayers().size() < mapConfig.getMinPlayers()) {
                SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.RED + "Not enough players to start the game!");
                SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.BLOCK_DISPENSER_DISPENSE, 0);
                stopCountdown();
            }
            return true;
        }

        return false;
    }

    /**
     * Forces the game to start if the game is able to start effectively overriding the countdown.
     * @return Returns true if the game started.
     */
    public boolean forceStartGame() {
        if (gameCountDown) {
            stopCountdown();
            SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.ENTITY_ARROW_HIT_PLAYER, 1);
            gameOpen = false;
            gameStarted = true;
            startGame();
            return true;
        }
        return false;
    }

    /**
     * Start the countdown that starts the game.
     */
    protected void startCountdown() {
        // Only start if the game hasn't started and a countdown isn't running.
        if (!gameStarted && !gameCountDown) {
            gameCountDown = true;
            countDownTime = 30;

            // Only create task if plugin is enabled. (used to prevent errors when plugin shuts down)
            if (SimplePVP.getPlugin().isEnabled()) {
                countTask = Bukkit.getScheduler().runTaskTimer(SimplePVP.getPlugin(), () -> {

                    if (countDownTime == 0) {
                        SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.ENTITY_ARROW_HIT_PLAYER, 1);
                        gameOpen = false;
                        gameStarted = true;
                        startGame();
                        stopCountdown();
                    } else if (countDownTime == 1) {
                        SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GREEN + "Starting game in " +
                                ChatColor.RED + countDownTime + ChatColor.GREEN + " second.");
                        SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.BLOCK_DISPENSER_DISPENSE, 2);
                    } else if (countDownTime == 30 || countDownTime == 20 || countDownTime == 10 || countDownTime <= 5) {
                        SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GREEN + "Starting game in " +
                                ChatColor.RED + countDownTime + ChatColor.GREEN + " seconds.");
                        SimpleUtil.playPlayersGlobalSound(getPlayers(), Sound.BLOCK_DISPENSER_DISPENSE, 2);
                    }

                    countDownTime--; // Decrement the countdown time every cycle.

                }, 0, 20);
            }
        }
    }

    /**
     * Stop the countdown that starts the game.
     */
    protected void stopCountdown() {
        gameCountDown = false;

        if (countTask != null) {
            countTask.cancel();
        }
    }

    /**
     * The start of the game. From here code everything you need to run the game.
     */
    protected abstract void startGame();

    /**
     * Call this whenever a game stat changes to see if the win condition is met.
     * This is automatically called by the Game class when a player leaves the game.
     */
    protected void checkGameWin() {
        if (gameStarted && players.isEmpty()) {
            resetGame(); // Reset when there are no more players left.
        }
    }

    /**
     * Ends the game.
     */
    protected void endGame() {
        if (gameStarted) {
            gameEnded = true;
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.BLUE + "Game resetting in 10 seconds.");

            // Set all players into spectator.
            for (Player gamePlayer : getPlayers()) {
                gamePlayer.setGameMode(GameMode.SPECTATOR);
            }

            // Reset the game after 10 seconds.
            if (SimplePVP.getPlugin().isEnabled()) {
                resetTask = Bukkit.getScheduler().runTaskLater(SimplePVP.getPlugin(), this::resetGame, 200);
            }
        }
    }

    /**
     * Resets the game.
     */
    protected void resetGame() {
        if (gameStarted) {
            // Send all players to the lobby.
            for (Player gamePlayer : new ArrayList<>(getPlayers())) {
                SimplePVP.sendToLobby(gamePlayer);
            }

            // Reset the game flags.
            gameOpen = true;
            gameCountDown = false;
            gameStarted = false;
            gameEnded = false;
            if (resetTask != null) {
                resetTask.cancel();
            }
        }
    }

    /**
     * Simulates a player death. This should be handled by the game to respawn the player, adjust game scores, etc.
     * @param player The player that died.
     * @param damager The damager that killed the player. This is null if there was no damager.
     * @param cause The cause of the damage that killed the player. This can sometimes be null.
     */
    protected void killPlayer(Player player, Player damager, EntityDamageEvent.DamageCause cause) {
        SimpleUtil.cleanPlayer(player);
        player.setGameMode(GameMode.SPECTATOR);

        if (damager != null) {
            SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " was killed by " + damager.getName() + ".");
        } else {
            if (cause != null) {
                switch (cause) {
                    case SUFFOCATION -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " suffocated in a wall.");
                    }
                    case FALL -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " fell from a high place.");
                    }
                    case FIRE, FIRE_TICK -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " burned to death.");
                    }
                    case LAVA -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " tried to swim in lava.");
                    }
                    case DROWNING -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " drowned.");
                    }
                    case BLOCK_EXPLOSION, ENTITY_EXPLOSION -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " exploded.");
                    }
                    case VOID -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " fell out of the world.");
                    }
                    case LIGHTNING -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " was struck by lightning.");
                    }
                    case MAGIC -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " was killed by magic.");
                    }
                    case WITHER -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " withered away.");
                    }
                    case FLY_INTO_WALL -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " hit the ground to hard.");
                    }
                    case FREEZE -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " froze to death.");
                    }
                    default -> {
                        SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " died.");
                    }
                }
            } else {
                SimpleUtil.sendPlayersMessage(getPlayers(), player.getName() + " died.");
            }
        }

        checkGameWin();
    }

    /**
     * This should respawn the player at their spawn location in the proper game mode. This should also give the player any items they need.
     * If respawning is not part of the game, leave this method empty.
     */
    protected abstract void respawnPlayer(Player player);

    /**
     * Get all the players in this game.
     * @return Returns a list of players in the game.
     */
    public List<Player> getPlayers() {
        return players;
    }

    public boolean hasPlayer(Player player) {
        return players.contains(player);
    }

    public String getGameName() {
        return gameName;
    }

    public Material getGameIcon() {
        return gameIcon;
    }

    public MapConfig getMapConfig() {
        return mapConfig;
    }

    public boolean isGameOpen() {
        return gameOpen;
    }

    @EventHandler
    public void onPlayerNearDeathEntity(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player player) {
            if (hasPlayer(player) && gameStarted && (player.getHealth() - e.getDamage()) <= 0) {
                e.setCancelled(true); // Player will die so cancel this
                if (e.getDamager() instanceof Player damager) {
                    killPlayer(player, damager, e.getCause());
                } else {
                    killPlayer(player, null, e.getCause());
                }
            }
        }
    }

    @EventHandler
    public void onPlayerNearDeathBlock(EntityDamageByBlockEvent e) {
        if (e.getEntity() instanceof Player player) {
            if (hasPlayer(player) && gameStarted && (player.getHealth() - e.getDamage()) <= 0) {
                e.setCancelled(true); // Player will die so cancel this
                killPlayer(player, null, e.getCause());
            }
        }
    }
}